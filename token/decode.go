package token

import (
	"strings"

	"gitlab.com/eddypastika-go/utilities/varis"

	"github.com/dgrijalva/jwt-go"
)

func (t *token) DecodeToken(tokenString string) (*AccessTokenClaims, error) {
	tokenString = strings.ReplaceAll(tokenString, "Bearer ", "")

	to, err := jwt.ParseWithClaims(tokenString, &AccessTokenClaims{}, t.keyFunc)
	if err != nil {
		return nil, err
	}

	claims, ok := to.Claims.(*AccessTokenClaims)
	if ok && to.Valid {
		return claims, nil
	}

	return nil, varis.ErrorInvalidToken
}

func (t *token) DecodeRefreshToken(refreshTokenString string) (*RefreshTokenClaims, error) {
	to, err := jwt.ParseWithClaims(refreshTokenString, &RefreshTokenClaims{}, t.keyFunc)
	if err != nil {
		return nil, err
	}

	claims, ok := to.Claims.(*RefreshTokenClaims)
	if ok && to.Valid {
		return claims, nil
	}

	return nil, varis.ErrorInvalidToken
}

func (t *token) keyFunc(token *jwt.Token) (interface{}, error) {
	_, ok := token.Method.(*jwt.SigningMethodHMAC)
	if !ok {
		return nil, varis.ErrorInvalidTokenSigningMethod
	}

	return []byte(t.secret), nil
}
