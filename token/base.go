package token

import (
	"github.com/dgrijalva/jwt-go"
	"gitlab.com/eddypastika-go/utilities/entitis"
)

type AccessTokenClaims struct {
	*jwt.StandardClaims
	User entitis.TokenPayload `json:"user"`
}

type RefreshTokenClaims struct {
	*jwt.StandardClaims
	UserID   uint64 `json:"id"`
	UserType string `json:"type"`
}

type Token interface {
	DecodeToken(tokenString string) (*AccessTokenClaims, error)
	DecodeRefreshToken(refreshTokenString string) (*RefreshTokenClaims, error)
}

type token struct {
	secret string
}

func New(secret string) Token {
	return &token{secret: secret}
}
