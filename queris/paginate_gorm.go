package queris

import (
	"math"
	"strings"

	"gitlab.com/eddypastika-go/utilities/varis"

	"github.com/labstack/echo/v4"
	"gitlab.com/eddypastika-go/utilities/common/typeconv"
	"gorm.io/gorm"
)

const (
	Ascending  = "ASC"
	Descending = "DESC"
	CreatedAt  = "created_at"
)

type PaginationRes struct {
	Count       int64       `json:"count"`
	CurrentPage int         `json:"currentPage"`
	TotalPages  int         `json:"totalPages"`
	Params      *Paging     `json:"params"`
	Items       interface{} `json:"items,omitempty"`
}

type Paging struct {
	Page  int    `json:"page"`
	Limit int    `json:"limit"`
	Sort  string `json:"sort"`
}

func (p *Paging) FromContext(c echo.Context) *Paging {
	p.Page = typeconv.ConvertStringToInt(c.QueryParam("page"))
	p.Limit = typeconv.ConvertStringToInt(c.QueryParam("limit"))
	p.Sort = c.QueryParam("sort")

	return p
}

func (p *Paging) Paginate(sortList map[string]string, count int64, res *PaginationRes) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		if p.Page == 0 {
			p.Page = 1
		}

		switch {
		case p.Limit > 100:
			p.Limit = 100
		case p.Limit <= 0:
			p.Limit = 10
		}

		res.Params = p
		res.Count = count
		res.CurrentPage = p.Page
		res.TotalPages = int(math.Ceil(float64(count) / float64(p.Limit)))

		if res.TotalPages < res.CurrentPage {
			res.CurrentPage = res.TotalPages
			p.Page = res.TotalPages
		}

		sort := setSortBy(p.Sort, sortList)
		if sort != "" {
			res.Params.Sort = sort
			db.Order(sort)
		}

		offset := (p.Page - 1) * p.Limit
		return db.Offset(offset).Limit(p.Limit)
	}
}

func setSortBy(sortKey string, sortList map[string]string) (sorts string) {
	sorts = CreatedAt + varis.Space + Descending

	if sortKey == varis.Empty {
		sortKey = CreatedAt
	}

	sortReq := strings.Split(sortKey, varis.Coma)

	var temp []string
	for _, v := range sortReq {
		direction := Ascending
		key := strings.TrimSpace(v)
		if key[0] == varis.Minus {
			direction = Descending
			key = key[1:]
		}

		if sortList[key] != varis.Empty {
			temp = append(temp, sortList[key]+varis.Space+direction)
		}
	}

	if len(temp) > 0 {
		sorts = strings.Join(temp, varis.Coma)
	}

	return
}
