package isession

import (
	"fmt"

	"github.com/labstack/echo/v4"
	"github.com/spf13/cast"
	"gitlab.com/eddypastika-go/utilities/entitis"
	"gitlab.com/eddypastika-go/utilities/logger"
)

func SkipLogging(url string) bool {
	urls := []string{"", "/", "/ping", "/health", "/routes"}

	for _, s := range urls {
		if s == url {
			return true
		}
	}

	return false
}

func FormatHeader(c echo.Context) (result map[string]string) {
	result = make(map[string]string)
	if headers := c.Request().Header; headers != nil {
		for k, v := range headers {
			result[k] = fmt.Sprintf("%v", v)
		}

		return result
	}

	return
}

func (is *Isession) getResponseCode(response interface{}) (rc string) {
	if def, ok := response.(entitis.Response); ok {
		return def.Status
	}

	return is.ResponseCode
}

func formatLogs(message ...interface{}) (logRecord []logger.Field) {
	for index, msg := range message {
		field := logger.Field{
			Key: "msg_" + cast.ToString(index),
			Val: msg,
		}
		logRecord = append(logRecord, field)
	}

	return
}
