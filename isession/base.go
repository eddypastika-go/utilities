package isession

import (
	"context"
	"net/http"
	"time"

	"gitlab.com/eddypastika-go/utilities/varis"

	"github.com/labstack/echo/v4"
	"gitlab.com/eddypastika-go/utilities/logger"
)

const KeyIsession = "Isession"

type Isession struct {
	Ctx                        context.Context
	Echo                       echo.Context
	Logger                     logger.Logger
	RequestTime                time.Time
	XID, Source                string
	AppName, AppVersion, IP    string
	Port, HTTPStatusCode       int
	SrcIP, URL, Method         string
	Header, Request, Response  interface{}
	ErrorMessage               string
	ResponseCode               string
	DBDialect                  string
	ServiceCode, ServiceSecret string
}

func NewFromEcho(ec echo.Context) (isession *Isession) {
	ctx := ec.Get(KeyIsession)
	isession, ok := ctx.(*Isession)
	if !ok {
		isession.ErrorMessage = varis.ErrorInvalidContext.Error()
		isession.ResponseCode = varis.CodeServerError
		isession.HTTPStatusCode = http.StatusInternalServerError
		return
	}

	return
}
