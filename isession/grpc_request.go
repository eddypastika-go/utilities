package isession

import (
	"context"

	"gitlab.com/eddypastika-go/utilities/common/typeconv"

	"gitlab.com/eddypastika-go/utilities/random"
	"gitlab.com/eddypastika-go/utilities/varis"
)

func SetGrpcCtx(c context.Context, xid, appCode string, req interface{}) *Isession {
	requestID := xid
	if requestID == "" {
		requestID = random.GenerateXID(appCode)
	}

	ctx := c.Value(KeyIsession).(*Isession)
	ctx.XID = requestID
	ctx.ResponseCode = varis.CodeSuccess
	ctx.Ctx = c

	ctx.Info("incoming gRPC request", typeconv.ToJSONIndent(req))
	return ctx
}
