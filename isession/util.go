package isession

import (
	"fmt"
	"net/http"
	"runtime"

	"gitlab.com/eddypastika-go/utilities/entitis"
	"gitlab.com/eddypastika-go/utilities/logger"

	"github.com/lib/pq"
	"gitlab.com/eddypastika-go/utilities/varis"
)

func GetSource() logger.Field {
	_, file, line, ok := runtime.Caller(1)
	if ok {
		return logger.Field{Key: "source", Val: fmt.Sprintf("[%s:%d]", file, line)}
	}

	return logger.Field{}
}

func (is *Isession) handleErrorDB(dbError *pq.Error) {
	is.ErrorMessage = dbError.Detail
	if is.ErrorMessage == "" {
		is.ErrorMessage = dbError.Message
	}
	if is.ErrorMessage == "" {
		is.ErrorMessage = varis.MsgDatabaseError
	}

	switch dbError.Code {
	case varis.PostgresDuplicateCode:
		is.ResponseCode = varis.CodeDBConflict
		is.HTTPStatusCode = http.StatusConflict
	case varis.PostgresUndefinedTable:
		is.ResponseCode = varis.CodeDBInvalidTable
		is.HTTPStatusCode = http.StatusInternalServerError
	case varis.PostgresDataTruncation:
		is.ResponseCode = varis.CodeDBDataTruncation
		is.HTTPStatusCode = http.StatusBadRequest
	case varis.PostgresCheckViolation:
		is.ResponseCode = varis.CodeDBDataTruncation
		is.HTTPStatusCode = http.StatusBadRequest
	default:
		is.ResponseCode = varis.CodeDBError
		is.HTTPStatusCode = http.StatusInternalServerError
	}
}

func (is *Isession) GetUserLogin() (login entitis.TokenPayload, err error) {
	login, ok := is.Echo.Get(varis.TokenPayload).(entitis.TokenPayload)
	if !ok {
		err = varis.ErrorInvalidUserLogin
		is.ResponseCode = varis.CodeTokenInvalidUserLogin
		is.HTTPStatusCode = http.StatusForbidden
		is.ErrorMessage = varis.ErrorInvalidUserLogin.Error()
	}

	return
}

func (is *Isession) ClearError() {
	is.ResponseCode = varis.CodeSuccess
	is.ErrorMessage = varis.Empty
	is.HTTPStatusCode = http.StatusOK
}
