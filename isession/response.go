package isession

import (
	"net/http"

	"gitlab.com/eddypastika-go/utilities/entitis"
	"gitlab.com/eddypastika-go/utilities/varis"
)

func (is *Isession) JSONResponse(data interface{}) error {
	var res entitis.Response

	httpStatusCode := is.HTTPStatusCode
	if httpStatusCode == 0 {
		httpStatusCode = http.StatusOK
	}

	res.Data = data
	res.Message = varis.MsgSuccess
	res.Status = is.ResponseCode

	if is.ErrorMessage != "" {
		res.Message = is.ErrorMessage
		res.Data = nil
	}

	if res.Status == "" {
		res.Status = varis.CodeSuccess
	}

	return is.Echo.JSON(httpStatusCode, res)
}

func (is *Isession) JSONError() error {
	return is.JSONResponse(nil)
}

func (is *Isession) JSONNoContent() error {
	is.ResponseCode = varis.CodeNoContent
	return is.JSONResponse(nil)
}
