package isession

import (
	"fmt"
	"net/http"
	"strings"
	"time"

	"gitlab.com/eddypastika-go/utilities/varis"

	"github.com/lib/pq"
	"gitlab.com/eddypastika-go/utilities/logger"
)

const (
	Success = "SUCCESS"
	Warning = "WARNING"
)

func (is *Isession) Error(source logger.Field, msg, code string, err error) {
	if code != "" {
		is.ResponseCode = code
	}

	if err != nil {
		is.ErrorMessage = err.Error()
	}

	if strings.Contains(is.ResponseCode, "IS4") {
		is.HTTPStatusCode = http.StatusBadRequest
	}

	// - handle error db postgres
	dbError, ok := err.(*pq.Error)
	if ok {
		is.handleErrorDB(dbError)
	}

	if is.ResponseCode == "" {
		is.ResponseCode = varis.CodeGeneralError
	}

	if is.ErrorMessage == "" {
		is.ErrorMessage = varis.ErrorGeneral.Error()
	}

	if is.HTTPStatusCode == 0 {
		is.HTTPStatusCode = http.StatusInternalServerError
	}

	logRecord := []logger.Field{
		{"xid", is.XID},
		{"method", is.Method},
		{"url", is.URL},
		{"rescode", is.ResponseCode},
		{"error", is.ErrorMessage},
		source,
	}

	is.Logger.Error(msg, logRecord...)
}

func (is *Isession) Info(msg string, message ...interface{}) {
	logRecord := []logger.Field{
		{"xid", is.XID},
		{"method", is.Method},
		{"url", is.URL},
	}

	addMsg := formatLogs(message...)
	logRecord = append(logRecord, addMsg...)

	is.Logger.Info(msg, logRecord...)
}

func (is *Isession) LOGIS(msg string, content ...interface{}) {
	stop := time.Now()
	rt := stop.Sub(is.RequestTime).Nanoseconds() / 1000000

	logRecord := []logger.Field{
		{"xid", is.XID},
		{"method", is.Method},
		{"url", is.URL},
		{"req", is.Request},
		{"res_time", fmt.Sprintf("%d ms", rt)},
	}

	addMsg := formatLogs(content...)
	logRecord = append(logRecord, addMsg...)

	is.Logger.Info(msg, logRecord...)

	response := is.Response
	if len(content) > 0 {
		response = content[0]
	}

	info := Success
	if is.HTTPStatusCode > 204 || !strings.Contains(is.ResponseCode, "IS0") {
		info = Warning
	}

	is.Logger.LOGIS(logger.Logis{
		Info:           info,
		AppName:        is.AppName,
		AppVersion:     is.AppVersion,
		IP:             is.IP,
		Port:           is.Port,
		SrcIP:          is.SrcIP,
		RespTime:       rt,
		Path:           fmt.Sprintf("[%v] %v", strings.ToUpper(is.Method), is.URL),
		Method:         strings.ToUpper(is.Method),
		Header:         is.Header,
		Request:        is.Request,
		Response:       response,
		Error:          is.ErrorMessage,
		XID:            is.XID,
		AdditionalData: nil,
		ResponseCode:   is.getResponseCode(response),
	})
}
