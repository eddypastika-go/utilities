package isession

import (
	"encoding/json"
	"io/ioutil"
	"reflect"

	"gitlab.com/eddypastika-go/utilities/varis"
)

func (is *Isession) ProcessRequest(msgLog string, req interface{}) (err error) {
	val := reflect.ValueOf(req)
	if val.Elem().Kind() == reflect.Slice || val.Elem().Kind() == reflect.Array {
		return is.processReqArray(msgLog, req)
	}

	if is.ErrorMessage != varis.Empty {
		is.Error(GetSource(), msgLog, varis.CodeInvalidContext, varis.ErrorInvalidContext)
		return varis.ErrorInvalidContext
	}

	if err = is.Echo.Bind(req); err != nil {
		is.Error(GetSource(), msgLog, varis.CodeBindError, err)
		return err
	}

	if err = is.Echo.Validate(req); err != nil {
		is.Error(GetSource(), msgLog, varis.CodeValidateError, err)
		return err
	}

	return
}

func (is *Isession) processReqArray(msgLog string, req interface{}) error {
	result, err := ioutil.ReadAll(is.Echo.Request().Body)
	if err != nil {
		is.Error(GetSource(), msgLog, varis.CodeBindError, err)
		return err
	}

	err = json.Unmarshal(result, &req)
	if err != nil {
		is.Error(GetSource(), msgLog, varis.CodeUnmarshalJson, err)
		return err
	}

	return nil
}

func (is *Isession) GetServiceCredential() (code, secret string) {
	code = is.Echo.Request().Header.Get(varis.ServiceCode)
	secret = is.Echo.Request().Header.Get(varis.ServiceSecret)

	return
}
