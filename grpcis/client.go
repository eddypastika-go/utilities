package grpcis

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/eddypastika-go/utilities/common"

	"gitlab.com/eddypastika-go/utilities/isession"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

var msgLog = "GRPC Client"

const XRequestID = "RequestID"

type RpcConnection struct {
	options Options
	Conn    *grpc.ClientConn
}

func (rpc *RpcConnection) CreateContext(parent context.Context, session *isession.Isession) (ctx context.Context) {
	ctx, _ = context.WithTimeout(parent, rpc.options.Timeout)
	ctx = context.WithValue(ctx, isession.KeyIsession, session)

	return
}

func NewGRPCConnection(options Options) *RpcConnection {
	var (
		conn *grpc.ClientConn
		err  error
	)

	conn, err = grpc.Dial(options.Address, grpc.WithInsecure(), withClientUnaryInterceptor())
	if err != nil {
		panic(err)
	}

	return &RpcConnection{
		Conn:    conn,
		options: options,
	}
}

func NewGRPCConnectionE(options Options) (rpc *RpcConnection, err error) {
	// todo still always insecure
	var conn *grpc.ClientConn

	conn, err = grpc.Dial(options.Address, grpc.WithInsecure(), withClientUnaryInterceptor())
	if err != nil {
		return
	}

	rpc = &RpcConnection{
		Conn:    conn,
		options: options,
	}

	return
}

func clientInterceptor(
	ctx context.Context,
	method string,
	req interface{},
	reply interface{},
	cc *grpc.ClientConn,
	invoker grpc.UnaryInvoker,
	opts ...grpc.CallOption,
) error {
	session := ctx.Value(isession.KeyIsession).(*isession.Isession)
	ctxWithMetadata := metadata.NewOutgoingContext(ctx, metadata.Pairs(XRequestID, session.XID))

	err := invoker(ctxWithMetadata, method, req, reply, cc, opts...)
	if err != nil {
		session.Error(isession.GetSource(), fmt.Sprintf("%s error", msgLog), method, err)
		return err
	}

	session.LOGIS(fmt.Sprintf("%s success", msgLog), common.FormatJsonLog(reply), method)

	return err
}

func withClientUnaryInterceptor() grpc.DialOption {
	return grpc.WithUnaryInterceptor(clientInterceptor)
}

type Options struct {
	Address      string        `json:"address"`
	Timeout      time.Duration `json:"timeout"`
	DebugMode    bool          `json:"debugMode"`
	WithProxy    bool          `json:"withProxy"`
	ProxyAddress string        `json:"proxyAddress"`
	SkipTLS      bool          `json:"skipTLS"`
}
