module gitlab.com/eddypastika-go/utilities

go 1.15

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-redis/redis/v8 v8.6.0
	github.com/gojektech/heimdall/v6 v6.1.0
	github.com/jonboulle/clockwork v0.2.2 // indirect
	github.com/labstack/echo/v4 v4.2.0
	github.com/lestrrat-go/file-rotatelogs v2.4.0+incompatible
	github.com/lestrrat-go/strftime v1.0.4 // indirect
	github.com/lib/pq v1.9.0
	github.com/mbndr/figlet4go v0.0.0-20190224160619-d6cef5b186ea
	github.com/oklog/ulid v1.3.1
	github.com/segmentio/encoding v0.2.7
	github.com/spf13/cast v1.3.1
	github.com/stretchr/testify v1.7.0
	go.uber.org/multierr v1.6.0 // indirect
	go.uber.org/zap v1.16.0
	golang.org/x/crypto v0.0.0-20200820211705-5c72a883971a
	golang.org/x/text v0.3.3
	google.golang.org/grpc v1.36.0
	gorm.io/gorm v1.21.3
)
