SHELL:=/bin/bash

lint:
	golangci-lint run --print-issued-lines=false --exclude-use-default=false --enable=golint --enable=goimports  --enable=unconvert --enable=unparam --enable=gosec --timeout=10m