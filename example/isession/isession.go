package main

import (
	"context"
	"fmt"
	"os"
	"time"

	"gitlab.com/eddypastika-go/utilities/entitis"

	"gitlab.com/eddypastika-go/utilities/isession"
	"gitlab.com/eddypastika-go/utilities/logger"
)

func main() {
	logs, err := setupLogger()
	if err != nil {
		panic(err)
	}

	is := isession.Isession{
		Ctx:          context.Background(),
		Logger:       logs,
		RequestTime:  time.Time{},
		XID:          "tested123",
		AppName:      "test",
		AppVersion:   "1.0.0",
		IP:           "123.123.123.123",
		Port:         8090,
		SrcIP:        "123.123.123.000",
		URL:          "https://test-is.id",
		Method:       "POST",
		ResponseCode: "",
		Response:     res,
	}

	//is.Error(isession.GetSource(), "test error", "400", errors.New("is error"))
	//is.Info("test error", "is info")
	is.LOGIS("tesssttt logis")
}

func setupLogger() (logger.Logger, error) {
	// this needed to make sure log file reside in current path
	dir, err := os.Getwd()
	if err != nil {
		_, _ = fmt.Fprintf(os.Stderr, "error get current path %s", err.Error())
		return nil, err
	}

	opt := logger.OptionsFile{
		Stdout:       false,
		FileLocation: fmt.Sprintf("%s/%s/log", dir, "tmp"),
		FileMaxAge:   time.Millisecond,
	}

	log := logger.SetupLoggerFile("test", &opt)

	return log, nil
}

var res = entitis.Response{
	Status:  "IS10",
	Message: "TESSTTT",
	Data:    nil,
}
