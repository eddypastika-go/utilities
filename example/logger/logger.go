package main

import (
	"fmt"
	"os"
	"time"

	"gitlab.com/eddypastika-go/utilities/logger"
)

func main() {
	// this needed to make sure log file reside in current path
	dir, err := os.Getwd()
	if err != nil {
		_, _ = fmt.Fprintf(os.Stderr, "error get current path %s", err.Error())
		return
	}

	opt := logger.OptionsFile{
		Stdout:       false,
		FileLocation: fmt.Sprintf("%s/%s/log", dir, "tmp"),
		FileMaxAge:   time.Millisecond,
	}

	log := logger.SetupLoggerFile("test", &opt)
	log.Info("info message")
	log.Error("error message")
	log.LOGIS(logger.Logis{})
}
