package main

import (
	"fmt"

	"gitlab.com/eddypastika-go/utilities/secret"
)

func main() {
	sec := secret.New()
	otp, err := sec.GenerateVerificationCode(6)
	if err != nil {
		panic(err)
	}

	fmt.Println("OTP: ", otp)
}
