package common

import (
	"fmt"
	"strings"

	"github.com/segmentio/encoding/json"
	"gitlab.com/eddypastika-go/utilities/isensor"
)

func StringBeautify(s string) string {
	return strings.ReplaceAll(strings.ReplaceAll(string(s), "\n", ""), " ", "")
}

func FormatInterfaceLog(b []byte) interface{} {
	var m interface{}

	err := json.Unmarshal(b, &m)
	if err != nil {
		return string(b)
	}

	return isensor.Sensor(m)
}

func FormatJsonLog(data interface{}) (res string) {
	data = isensor.Sensor(data)

	b, err := json.Marshal(data)
	if err != nil {
		res = fmt.Sprintf("%v", data)
		return
	}
	res = string(b)

	return
}
