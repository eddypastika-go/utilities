package typeconv

import (
	"strconv"
	"strings"

	"gitlab.com/eddypastika-go/utilities/varis"

	"github.com/segmentio/encoding/json"
)

func ToJSONIndent(data interface{}) string {
	b, err := json.MarshalIndent(data, "", "  ")
	if err != nil {
		return ""
	}

	return string(b)
}

func ConvertBoolToString(value bool) string {
	return strconv.FormatBool(value)
}

func ConvertStringToBool(value string) (bool, error) {
	return strconv.ParseBool(value)
}

func ConvertUint64ToString(value uint64) string {
	return strconv.FormatUint(value, 10)
}

func ConvertIntToString(value int) string {
	return strconv.Itoa(value)
}

func ConvertStringToInt(value string) int {
	out, err := strconv.Atoi(value)
	if err != nil {
		out = 0
	}

	return out
}

func ConvertStringToUint64(value string) uint64 {
	out := ConvertStringToInt(value)

	return uint64(out)
}

func ConvertStringToUint16(value string) uint16 {
	out := ConvertStringToInt(value)

	return uint16(out)
}

func ConvertEnvToArrayString(env string) []string {
	return strings.Split(env, ",")
}

func ConvertQueryParamToIDs(s string) []uint64 {
	list := strings.Split(s, varis.Coma)

	ids := make([]uint64, len(list))
	for i, id := range list {
		ids[i] = ConvertStringToUint64(id)
	}

	return ids
}
