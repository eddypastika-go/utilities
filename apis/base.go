package apis

import "time"

type (
	Apis interface {
		Create(timeout time.Duration) Client
		CreateWithRetry(timeout time.Duration, retryCount int) Client
	}
)
