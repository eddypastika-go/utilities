package heimdall

import (
	"time"

	"github.com/gojektech/heimdall/v6/httpclient"
	"gitlab.com/eddypastika-go/utilities/apis"
)

type (
	api struct{}
)

func (a *api) Create(timeout time.Duration) apis.Client {
	return &client{Doer: *httpclient.NewClient(httpclient.WithHTTPTimeout(timeout))}
}

func (a *api) CreateWithRetry(timeout time.Duration, retryCount int) apis.Client {
	return &client{Doer: *httpclient.NewClient(
		httpclient.WithHTTPTimeout(timeout),
		httpclient.WithRetryCount(retryCount),
	)}
}

func New() apis.Apis {
	return &api{}
}
