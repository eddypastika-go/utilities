package heimdall

import (
	"fmt"
	"io"
	"net/http"

	"gitlab.com/eddypastika-go/utilities/varis"

	neturl "net/url"

	"github.com/gojektech/heimdall/v6/httpclient"
	"gitlab.com/eddypastika-go/utilities/isession"
	"golang.org/x/text/encoding"
)

var msgLog = "HTTP Client"

type (
	client struct {
		Doer httpclient.Client
		json encoding.Encoding
	}
)

func (c *client) Get(
	ctx *isession.Isession, url string, headers http.Header, queryString map[string]string,
) (res *http.Response, err error) {
	if len(queryString) > 0 {
		for key, value := range queryString {
			url += fmt.Sprintf("?%s=%s", key, neturl.QueryEscape(value))
		}
	}

	res, err = c.Doer.Get(url, headers)
	if err != nil {
		ctx.Error(isession.GetSource(), msgLog, varis.CodeHTTPErrorGet, err)
	}

	return
}

func (c *client) Post(
	ctx *isession.Isession, url string, body io.Reader, headers http.Header,
) (res *http.Response, err error) {
	res, err = c.Doer.Post(url, body, headers)
	if err != nil {
		ctx.Error(isession.GetSource(), msgLog, varis.CodeHTTPErrorPost, err)
	}

	return
}

func (c *client) Put(
	ctx *isession.Isession, url string, body io.Reader, headers http.Header,
) (res *http.Response, err error) {
	res, err = c.Doer.Put(url, body, headers)
	if err != nil {
		ctx.Error(isession.GetSource(), msgLog, varis.CodeHTTPErrorPut, err)
	}

	return
}

func (c *client) Patch(
	ctx *isession.Isession, url string, body io.Reader, headers http.Header,
) (res *http.Response, err error) {
	res, err = c.Doer.Patch(url, body, headers)
	if err != nil {
		ctx.Error(isession.GetSource(), msgLog, varis.CodeHTTPErrorPatch, err)
	}

	return
}

func (c *client) Delete(
	ctx *isession.Isession, url string, headers http.Header,
) (res *http.Response, err error) {
	res, err = c.Doer.Delete(url, headers)
	if err != nil {
		ctx.Error(isession.GetSource(), msgLog, varis.CodeHTTPErrorDelete, err)
	}

	return
}

func (c *client) Do(ctx *isession.Isession, req *http.Request) (res *http.Response, err error) {
	res, err = c.Doer.Do(req)
	if err != nil {
		ctx.Error(isession.GetSource(), msgLog, varis.CodeHTTPError, err)
	}

	return
}
