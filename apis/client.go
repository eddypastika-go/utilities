package apis

import (
	"io"
	"net/http"

	"gitlab.com/eddypastika-go/utilities/isession"
)

type (
	Client interface {
		Get(ctx *isession.Isession, url string, headers http.Header, queryString map[string]string) (*http.Response, error)
		Post(ctx *isession.Isession, url string, body io.Reader, headers http.Header) (*http.Response, error)
		Put(ctx *isession.Isession, url string, body io.Reader, headers http.Header) (*http.Response, error)
		Patch(ctx *isession.Isession, url string, body io.Reader, headers http.Header) (*http.Response, error)
		Delete(ctx *isession.Isession, url string, headers http.Header) (*http.Response, error)
		Do(ctx *isession.Isession, req *http.Request) (*http.Response, error)
	}
)
