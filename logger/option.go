package logger

import "time"

type OptionsFile struct {
	Stdout       bool          `json:"stdout"`
	FileLocation string        `json:"fileLocation"`
	FileMaxAge   time.Duration `json:"fileMaxAge"`
	Mask         bool          `json:"mask"`
}
