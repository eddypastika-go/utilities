package logger

type Logger interface {
	Debug(message interface{}, fields ...Field)
	DebugWithMetadata(threadID, tag, method, uri string, fields ...Field)
	Info(message interface{}, fields ...Field)
	InfoWithMetadata(threadID, tag, method, uri string, fields ...Field)
	Warn(message interface{}, fields ...Field)
	WarnWithMetadata(threadID, tag, method, uri string, fields ...Field)
	Error(message interface{}, fields ...Field)
	ErrorWithMetadata(threadID, tag, method, uri string, fields ...Field)
	Fatal(message interface{}, fields ...Field)
	FatalWithMetadata(threadID, tag, method, uri string, fields ...Field)
	Panic(message interface{}, fields ...Field)
	PanicWithMetadata(threadID, tag, method, uri string, fields ...Field)
	LOGIS(logis Logis)
}

type Field struct {
	Key string
	Val interface{}
}

type Logis struct {
	AppName        string      `json:"app"`
	AppVersion     string      `json:"version"`
	IP             string      `json:"ip"`
	Port           int         `json:"port"`
	SrcIP          string      `json:"src_ip"`
	RespTime       int64       `json:"rt"`
	Path           string      `json:"path"`
	Header         interface{} `json:"header"` // better to pass data here as is, don't cast it to string. use map or array
	Request        interface{} `json:"req"`
	Response       interface{} `json:"resp"`
	Error          string      `json:"error"`
	XID            string      `json:"xid"`
	AdditionalData interface{} `json:"add_data"`
	ResponseCode   string      `json:"rc"`
	HTTPCode       int         `json:"http_code"`
	Method         string      `json:"method"`
	Info           string      `json:"INFO"`
}
