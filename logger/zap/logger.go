package zap

import (
	"os"
	"time"

	rotatelogs "github.com/lestrrat-go/file-rotatelogs"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func NewZapLogger(stdout bool, path string, maxAge time.Duration) (logger *zap.Logger) {
	if !stdout && maxAge <= 0 {
		panic("max age is zero")
	}

	writer := zapcore.AddSync(os.Stdout)
	if !stdout {
		rotate, err := rotatelogs.New(
			path+".%Y%m%d",
			rotatelogs.WithLinkName(path),
			rotatelogs.WithMaxAge(maxAge*24*time.Hour),
			rotatelogs.WithRotationTime(time.Hour),
		)
		if err != nil {
			panic(err)
		}
		writer = zapcore.AddSync(rotate)
	}

	core := zapcore.NewCore(getEncoder(), writer, zapcore.InfoLevel)
	logger = zap.New(core)

	return
}

func getEncoder() zapcore.Encoder {
	encoderConfig := zapcore.EncoderConfig{
		TimeKey:        "ts",
		MessageKey:     "level",
		EncodeDuration: millisDurationEncoder,
		EncodeTime:     timeEncoder,
		LineEnding:     zapcore.DefaultLineEnding,
	}

	return zapcore.NewJSONEncoder(encoderConfig)
}

func timeEncoder(t time.Time, enc zapcore.PrimitiveArrayEncoder) {
	enc.AppendString(t.Format("2006-01-02 15:04:05.999"))
}

func millisDurationEncoder(d time.Duration, enc zapcore.PrimitiveArrayEncoder) {
	enc.AppendInt64(d.Nanoseconds() / 1000000)
}
