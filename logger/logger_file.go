package logger

import (
	"encoding/json"
	"reflect"

	"github.com/segmentio/encoding/proto"
	logzap "gitlab.com/eddypastika-go/utilities/logger/zap"
	"go.uber.org/zap"
)

const (
	info     = "[INFO]"
	errorLvl = "[ERROR]"
	fatal    = "[FATAL]"
	warning  = "[WARNING]"
	logis    = "[LOGIS]"
)

type fileLogger struct {
	serviceName string
	logger      *zap.Logger
	mask        bool
}

func SetupLoggerFile(serviceName string, config *OptionsFile) Logger {
	return &fileLogger{
		serviceName: serviceName,
		logger:      logzap.NewZapLogger(config.Stdout, config.FileLocation, config.FileMaxAge),
		mask:        config.Mask,
	}
}

func (l *fileLogger) Debug(message interface{}, fields ...Field) {
	fields = append(fields, ToDefaultField(message))
	logRecord := l.formatLogs(fields...)
	l.logger.Debug(info, logRecord...)
}

func (l *fileLogger) DebugWithMetadata(xID, tag, method, uri string, fields ...Field) {
	l.logger.Debug(info, l.logFormat(xID, tag, method, uri, fields...)...)
}

func (l *fileLogger) Info(message interface{}, fields ...Field) {
	fields = append(fields, ToDefaultField(message))
	logRecord := l.formatLogs(fields...)
	l.logger.Info(info, logRecord...)
}

func (l *fileLogger) InfoWithMetadata(xID, tag, method, uri string, fields ...Field) {
	l.logger.Info(info, l.logFormat(xID, tag, method, uri, fields...)...)
}

func (l *fileLogger) Warn(message interface{}, fields ...Field) {
	fields = append(fields, ToDefaultField(message))
	logRecord := l.formatLogs(fields...)
	l.logger.Warn(warning, logRecord...)
}

func (l *fileLogger) WarnWithMetadata(xID, tag, method, uri string, fields ...Field) {
	l.logger.Warn(warning, l.logFormat(xID, tag, method, uri, fields...)...)
}

func (l *fileLogger) Error(message interface{}, fields ...Field) {
	fields = append(fields, ToDefaultField(message))
	logRecord := l.formatLogs(fields...)
	l.logger.Error(errorLvl, logRecord...)
}

func (l *fileLogger) ErrorWithMetadata(xID, tag, method, uri string, fields ...Field) {
	l.logger.Error(errorLvl, l.logFormat(xID, tag, method, uri, fields...)...)
}

func (l *fileLogger) Fatal(message interface{}, fields ...Field) {
	fields = append(fields, ToDefaultField(message))
	logRecord := l.formatLogs(fields...)
	l.logger.Fatal(fatal, logRecord...)
}

func (l *fileLogger) FatalWithMetadata(xID, tag, method, uri string, fields ...Field) {
	l.logger.Fatal(fatal, l.logFormat(xID, tag, method, uri, fields...)...)
}

func (l *fileLogger) Panic(message interface{}, fields ...Field) {
	fields = append(fields, ToDefaultField(message))
	logRecord := l.formatLogs(fields...)
	l.logger.Panic(errorLvl, logRecord...)
}

func (l *fileLogger) PanicWithMetadata(xID, tag, method, uri string, fields ...Field) {
	l.logger.Panic(errorLvl, l.logFormat(xID, tag, method, uri, fields...)...)
}

func (l *fileLogger) LOGIS(model Logis) {
	l.logger.Info(
		logis,
		zap.String("info", model.Info),
		zap.String("app_name", l.serviceName),
		zap.String("xid", model.XID),
		zap.Int64("rt", model.RespTime),
		zap.Int("port", model.Port),
		zap.String("ip", model.IP),
		zap.String("app", model.AppName),
		zap.String("version", model.AppVersion),
		zap.String("path", model.Path),
		l.formatLog("header", model.Header, false),
		l.formatLog("req", model.Request, l.mask),
		l.formatLog("resp", model.Response, l.mask),
		zap.String("src_ip", model.SrcIP),
		zap.String("error", model.Error),
		l.formatLog("add_data", model.AdditionalData, false),
		zap.String("rc", model.ResponseCode),
		zap.String("method", model.Method),
	)
}

func (l *fileLogger) logFormat(xID, tag, method, uri string, fields ...Field) (logRecord []zap.Field) {
	logRecord = []zap.Field{
		zap.String("_app_name", l.serviceName),
		zap.String("_app_xid", xID),
		zap.String("_app_tag", tag),
		zap.String("_app_method", method),
		zap.String("_app_uri", uri),
	}

	mgs := l.formatLogs(fields...)
	logRecord = append(logRecord, mgs...)
	return
}

func (l *fileLogger) formatLogs(message ...Field) (logRecord []zap.Field) {
	for _, msg := range message {
		logRecord = append(logRecord, l.formatLog(msg.Key, msg.Val, false))
	}

	return
}

func (l *fileLogger) formatLog(key string, msg interface{}, mask bool) (logRecord zap.Field) {
	if msg == nil {
		logRecord = zap.Any(key, struct{}{})
		return
	}
	if p, ok := msg.(proto.Message); ok {
		if b, err := json.Marshal(p); err == nil {
			value := toJSON(string(b), mask)
			logRecord = zap.Any(key, value)
			return
		}
	}
	var value interface{}
	if mask {
		value = toJSON(msg, mask)
		if convert, ok := value.(reflect.Value); ok {
			value = convert.Interface()
		}
	}
	if !mask {
		value = toJSON(msg, mask)
	}

	logRecord = zap.Any(key, value)

	return
}
