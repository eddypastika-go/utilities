package random

import (
	"math/rand"
	"strings"
	"time"

	"github.com/oklog/ulid"
)

func GenerateXID(appCode string) string {
	t := time.Now()
	entropy := rand.New(rand.NewSource(t.UnixNano()))
	uniqueID := ulid.MustNew(ulid.Timestamp(t), entropy)
	return strings.ToUpper(appCode + uniqueID.String())
}

func GetMixedNumbAndLetter(n int) string {
	rand.Seed(time.Now().UnixNano())
	letterRunes := []rune("abcdefghijklmnopqrstuvwxyz01233456789")

	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}

	return string(b)
}
