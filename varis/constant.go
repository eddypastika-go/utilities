package varis

import (
	"errors"

	v8 "github.com/go-redis/redis/v8"
)

// - errors
var (
	ErrorGRPC                      = errors.New("grpc error")
	ErrorGRPCEmptyResult           = errors.New("grpc empty result")
	ErrorAuthorizationNotFound     = errors.New("authorization not found")
	ErrorUnauthorizedUser          = errors.New("unauthorized user")
	ErrorInvalidToken              = errors.New("invalid token")
	ErrorInvalidTokenSigningMethod = errors.New("invalid token signing method")
	ErrorInvalidContext            = errors.New("invalid context value")
	ErrorInvalidUserLogin          = errors.New("invalid user login")
	ErrorGeneral                   = errors.New("general error")
)

// - error cache
var (
	ErrorCacheNil           = v8.Nil
	ErrorCacheInvalidClient = errors.New("invalid cache client")
	ErrorCacheNoClient      = errors.New("no cache client")
	ErrorCacheFailedPing    = errors.New("cache ping failed")
	ErrorCacheKeyAlreadySet = errors.New("cache key already set")
	ErrorCacheEmptyAddress  = errors.New("addresses cannot be empty")
	ErrorCacheDelete        = errors.New("cache key didn't exists, or already deleted, or already expired")
)

// - endpoint type
const (
	// EndpointMancanegara is endpoint type that no need
	// any credential header to access it (PUBLIC)
	EndpointMancanegara = "MANCANEGARA"

	// EndpointDomestik is endpoint type that need
	// service_code, service_secret to access it
	// or NO need to login first to access the endpoint (TRUSTED)
	EndpointDomestik = "DOMESTIK"

	// EndpointLokal is endpoint type that need
	// service_code, service_secret and AUTHORIZATION to access it
	// or NEED to login first to access the endpoint (GRANTED)
	EndpointLokal = "LOKAL"
)

// - response code
const (
	// - success code
	CodeSuccess   = "IS00"
	CodeCreated   = "IS01"
	CodeUpdated   = "IS02"
	CodeNoContent = "IS04"

	// - error token code
	CodeTokenInvalid          = "IS10"
	CodeTokenErrorGenerate    = "IS11"
	CodeTokenExpired          = "IS12"
	CodeTokenInvalidUserLogin = "IS13"

	// - error password code
	CodePasswordErrorEmpty    = "IS20"
	CodePasswordErrorWrong    = "IS21"
	CodePasswordErrorGenerate = "IS22"

	// - error request code
	CodeBadRequest     = "IS40"
	CodeInvalidRequest = "IS41"
	CodeInvalidContext = "IS42"
	CodeUnauthorized   = "IS43"
	CodeNotFound       = "IS44"
	CodeBindError      = "IS45"
	CodeValidateError  = "IS46"
	CodeUnmarshalJson  = "IS47"

	// - error server code
	CodeServerError = "IS50"

	// - error db code
	CodeDBError          = "IS60"
	CodeDBErrorInsert    = "IS61"
	CodeDBErrorUpdate    = "IS62"
	CodeDBErrorDelete    = "IS63"
	CodeDBErrorGet       = "IS64"
	CodeDBConflict       = "IS65"
	CodeDBInvalidTable   = "IS66"
	CodeDBDataTruncation = "IS67"
	CodeDBCheckViolation = "IS68"
	CodeDBErrorUpsert    = "IS69"

	// - error cache code
	CodeCacheError            = "IS70"
	CodeCacheErrorGet         = "IS71"
	CodeCacheErrorSet         = "IS72"
	CodeCacheErrorSetIfAbsent = "IS73"
	CodeCacheErrorDelete      = "IS74"

	// - error grpcis
	CodeGRPCError       = "IS80"
	CodeGRPCEmptyResult = "IS81"

	// - general error
	CodeHTTPError       = "IS90"
	CodeHTTPErrorGet    = "IS91"
	CodeHTTPErrorPost   = "IS92"
	CodeHTTPErrorPut    = "IS93"
	CodeHTTPErrorPatch  = "IS94"
	CodeHTTPErrorDelete = "IS95"
	CodeGeneralError    = "IS99"
)

// - response message
const (
	MsgSuccess        = "Success"
	MsgInvalidRequest = "Invalid request"
	MsgDatabaseError  = "Database error"
)

// - db dialect
const (
	DBPostgres = "postgres"
	DBMySQL    = "mysql"
	DBMongo    = "mongo"
)

// - postgres error code
const (
	PostgresDuplicateCode  = "23505"
	PostgresUndefinedTable = "42P01"
	PostgresDataTruncation = "22001"
	PostgresCheckViolation = "23514"
)

// - general key
const (
	Authorization = "authorization"
	Bearer        = "Bearer "
	Source        = "source"
	ServiceCode   = "servicecode"
	ServiceSecret = "servicececret"
	TokenPayload  = "TOKEN_PAYLOAD"
)

// - Char
const (
	Empty = ""
	Space = " "
	Coma  = ","
)

// - Math Operator
const (
	Minus = '-'
)

const (
	FormatTimeDDMMYY         = "020106"
	FormatTimeYYYYmmDDhhMMss = "20060102150405"
)
