package entitis

import "strings"

type TokenPayload struct {
	ID    uint64
	Email string
	Type  string
}

type TokenPair struct {
	AccessToken  string
	RefreshToken string
}

func (tp *TokenPayload) IsUserEligible(email string) bool {
	return strings.ToLower(email) == strings.ToLower(tp.Email)
}
