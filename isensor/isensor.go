package isensor

import (
	"reflect"
	"strings"
)

var sensor = "********"

func Sensor(data interface{}) interface{} {
	v := reflect.ValueOf(&data)
	v2 := v.Elem()

	switch v2.Kind() {
	case reflect.Struct:
		sensorStruct(&v2)
	case reflect.Map:
		sensorMap(&v2)
	case reflect.Interface:
		sensorInterface(&v2)
	default:
		return data
	}

	return data
}

func sensorInterface(v *reflect.Value) {
	v2 := v.Elem()
	if v2.Kind() == reflect.Struct {
		sensorStruct(&v2)
	} else if v2.Kind() == reflect.Map {
		sensorMap(&v2)
	} else if v2.Kind() == reflect.Interface {
		sensorInterface(&v2)
	}
}

func sensorMap(v *reflect.Value) {
	maps := v.MapRange()
	for maps.Next() {
		name := strings.ToLower(maps.Key().String())
		if isSensitive(name) {
			nValue := reflect.ValueOf(sensor)
			v.SetMapIndex(maps.Key(), nValue)
		}
	}
	return
}

func sensorStruct(v *reflect.Value) {
	for i := 0; i < v.NumField(); i++ {
		name := strings.ToLower(v.Type().Field(i).Name)
		if isSensitive(name) {
			f := v.Field(i)
			if f.CanSet() {
				newVal := reflect.ValueOf(sensor)
				f.Set(newVal)
			}
		}
	}
}

func isSensitive(name string) bool {
	return name == "password" || name == "pwd" || name == "pass"
}
