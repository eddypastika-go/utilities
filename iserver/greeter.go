package iserver

import (
	"fmt"

	"github.com/mbndr/figlet4go"
)

type Option struct {
	Name, Desc, Version string
	HTTPPort, GRPCPort  int
}

func NewGreeter(opt Option) {
	ascii := figlet4go.NewAsciiRender()

	options := figlet4go.NewRenderOptions()
	options.FontColor = []figlet4go.Color{figlet4go.ColorMagenta}

	renderStr, _ := ascii.RenderOpts(opt.Name, options)
	fmt.Print(renderStr)
	fmt.Print(opt.Desc)
	fmt.Printf("\nv%s\n", opt.Version)
	fmt.Printf("\nHTTP listening on port: %v\n", opt.HTTPPort)
	fmt.Printf("\ngRPC listening on port: %v\n", opt.GRPCPort)
}
