package iserver

import (
	"net/http"
	"regexp"
	"sort"
	"strings"

	"gitlab.com/eddypastika-go/utilities/varis"

	"github.com/labstack/echo/v4"
	"gitlab.com/eddypastika-go/utilities/entitis"
)

type Endpoint struct {
	Method      string `json:"method"`
	Path        string `json:"path"`
	Type        string `json:"type"`
	ServiceName string `json:"service_name"`
}

func GetAllRoutes(e *echo.Echo, name string) {
	e.GET("/routes", func(c echo.Context) error {
		var (
			result  entitis.Response
			content []Endpoint
			types   = regexp.MustCompile(`MANCANEGARA|DOMESTIK|LOKAL`)
			methods = regexp.MustCompile(`POST|PUT|GET|DELETE|PATCH`)
			paths   = regexp.MustCompile(`/health|/ping|/routes`)
		)

		r := e.Routes()
		for _, v := range r {
			if types.MatchString(v.Name) && methods.MatchString(v.Method) &&
				!paths.MatchString(v.Path) && v.Path != "/" {
				en := Endpoint{
					Method:      v.Method,
					Path:        v.Path,
					Type:        v.Name,
					ServiceName: strings.ToLower(name),
				}
				content = append(content, en)
			}
		}

		sort.Slice(content, func(i, j int) bool {
			return r[i].Path < r[j].Path
		})

		result.Data = content
		result.Status = varis.CodeSuccess
		result.Message = varis.MsgSuccess

		return c.JSON(http.StatusOK, result)
	})
}
