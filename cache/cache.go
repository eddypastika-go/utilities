package cache

import (
	"time"

	"gitlab.com/eddypastika-go/utilities/isession"
)

type Cache interface {
	// Get looks for key and returns corresponding value to destination.
	Get(ctx *isession.Isession, key string, value interface{}) error
	// Set save key with value
	Set(ctx *isession.Isession, key string, value interface{}, expiration time.Duration) error
	// SetIfNotExists returns error if key already exists
	SetIfAbsent(
		ctx *isession.Isession,
		key string,
		value interface{},
		expiration time.Duration,
	) error
	// Delete deletes a key
	Delete(ctx *isession.Isession, key string) error
	Close(ctx *isession.Isession) error
}
