package redis

import (
	"context"

	"gitlab.com/eddypastika-go/utilities/varis"

	v8 "github.com/go-redis/redis/v8"
)

type Option func(*redis) error

func WithClient(client *v8.Client) Option {
	return func(r *redis) error {
		if r.mode == Single {
			r.client = client

			_, err := r.client.Ping(context.Background()).Result()
			if err != nil {
				return varis.ErrorCacheFailedPing
			}

			return nil
		}

		return varis.ErrorCacheInvalidClient
	}
}

func WithSentinelClient(client *v8.Client) Option {
	return func(r *redis) error {
		if r.mode == Sentinel {
			r.client = client

			_, err := r.client.Ping(context.Background()).Result()
			if err != nil {
				return varis.ErrorCacheFailedPing
			}

			return nil
		}

		return varis.ErrorCacheInvalidClient
	}
}

func WithClusterClient(client *v8.ClusterClient) Option {
	return func(r *redis) error {
		if r.mode == Cluster {
			r.client = client

			_, err := r.client.Ping(context.Background()).Result()
			if err != nil {
				return varis.ErrorCacheFailedPing
			}

			return nil
		}

		return varis.ErrorCacheInvalidClient
	}
}
