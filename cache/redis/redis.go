package redis

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/eddypastika-go/utilities/isession"

	"gitlab.com/eddypastika-go/utilities/varis"

	"gitlab.com/eddypastika-go/utilities/cache"

	v8 "github.com/go-redis/redis/v8"
	"github.com/segmentio/encoding/json"
)

type redis struct {
	debug  bool
	mode   Mode
	prefix string
	client client
}

func New(config Config, opts ...Option) (cache.Cache, error) {
	rdb := &redis{
		debug:  config.Debug,
		prefix: config.Prefix,
		mode:   config.Mode,
	}

	for i := range opts {
		if err := opts[i](rdb); err != nil {
			return nil, err
		}
	}

	if rdb.client == nil {
		if config.Mode == Single {
			if len(config.Addresses) == 0 {
				return nil, varis.ErrorCacheEmptyAddress
			}

			rdb.client = v8.NewClient(&v8.Options{
				Network:            config.Network,
				Addr:               config.Addresses[0],
				Username:           config.Username,
				Password:           config.Password,
				DB:                 config.DB,
				MaxRetries:         config.MaxRetries,
				MinRetryBackoff:    config.MinRetryBackoff,
				MaxRetryBackoff:    config.MaxRetryBackoff,
				DialTimeout:        config.DialTimeout,
				ReadTimeout:        config.ReadTimeout,
				WriteTimeout:       config.WriteTimeout,
				PoolSize:           config.PoolSize,
				MinIdleConns:       config.MinIdleConnections,
				MaxConnAge:         config.MaxConnectionAge,
				PoolTimeout:        config.PoolTimeout,
				IdleTimeout:        config.IdleTimeout,
				IdleCheckFrequency: config.IdleCheckFrequency,
			})
		}

		if config.Mode == Sentinel {
			if len(config.Addresses) == 0 {
				return nil, varis.ErrorCacheEmptyAddress
			}

			rdb.client = v8.NewFailoverClient(&v8.FailoverOptions{
				MasterName:         config.MasterName,
				SentinelAddrs:      config.Addresses,
				SentinelPassword:   config.SentinelPassword,
				RouteByLatency:     config.RouteByLatency,
				RouteRandomly:      config.RouteRandomly,
				SlaveOnly:          config.SlaveOnly,
				Username:           config.Username,
				Password:           config.Password,
				DB:                 config.DB,
				MaxRetries:         config.MaxRetries,
				MinRetryBackoff:    config.MinRetryBackoff,
				MaxRetryBackoff:    config.MaxRetryBackoff,
				DialTimeout:        config.DialTimeout,
				ReadTimeout:        config.ReadTimeout,
				WriteTimeout:       config.WriteTimeout,
				PoolSize:           config.PoolSize,
				MinIdleConns:       config.MinIdleConnections,
				MaxConnAge:         config.MaxConnectionAge,
				PoolTimeout:        config.PoolTimeout,
				IdleTimeout:        config.IdleTimeout,
				IdleCheckFrequency: config.IdleCheckFrequency,
			})
		}

		if config.Mode == Cluster {
			if len(config.Addresses) == 0 {
				return nil, varis.ErrorCacheEmptyAddress
			}

			rdb.client = v8.NewClusterClient(&v8.ClusterOptions{
				Addrs:              config.Addresses,
				MaxRedirects:       config.MaxRedirects,
				ReadOnly:           config.ReadOnly,
				RouteByLatency:     config.RouteByLatency,
				RouteRandomly:      config.RouteRandomly,
				Username:           config.Username,
				Password:           config.Password,
				MaxRetries:         config.MaxRetries,
				MinRetryBackoff:    config.MinRetryBackoff,
				MaxRetryBackoff:    config.MaxRetryBackoff,
				DialTimeout:        config.DialTimeout,
				ReadTimeout:        config.ReadTimeout,
				WriteTimeout:       config.WriteTimeout,
				PoolSize:           config.PoolSize,
				MinIdleConns:       config.MinIdleConnections,
				MaxConnAge:         config.MaxConnectionAge,
				PoolTimeout:        config.PoolTimeout,
				IdleTimeout:        config.IdleTimeout,
				IdleCheckFrequency: config.IdleCheckFrequency,
			})
		}
	}

	if rdb.client == nil {
		return nil, varis.ErrorCacheNoClient
	}

	if rdb.client != nil {
		if _, err := rdb.client.Ping(context.Background()).Result(); err != nil {
			return nil, varis.ErrorCacheFailedPing
		}
	}

	return rdb, nil
}

func (r *redis) Get(ctx *isession.Isession, key string, value interface{}) (err error) {
	defer func() {
		if err != nil {
			ctx.Error(isession.GetSource(), "Get Redis", varis.CodeCacheErrorGet, err)
		}
	}()

	get := r.client.Get(ctx.Ctx, r.key(key))

	if r.debug {
		fmt.Println(get.Args())
	}

	if get.Err() != nil {
		err = get.Err()
		return
	}

	data, _ := get.Bytes()

	err = json.Unmarshal(data, &value)

	return
}

func (r *redis) Set(
	ctx *isession.Isession,
	key string,
	value interface{},
	expiration time.Duration,
) (err error) {
	defer func() {
		if err != nil {
			ctx.Error(isession.GetSource(), "Set Redis", varis.CodeCacheErrorSet, err)
		}
	}()

	var valueBytes []byte
	if valueBytes, err = json.Marshal(value); err != nil {
		return
	}

	set := r.client.Set(ctx.Ctx, r.key(key), valueBytes, expiration)

	if r.debug {
		fmt.Println(set.Args())
	}

	if set.Err() != nil {
		err = set.Err()
		return
	}

	return
}

func (r *redis) SetIfAbsent(
	ctx *isession.Isession,
	key string,
	value interface{},
	expiration time.Duration,
) (err error) {
	defer func() {
		if err != nil {
			ctx.Error(isession.GetSource(), "SetIfAbsent Redis", varis.CodeCacheErrorSetIfAbsent, err)
		}
	}()

	var valueBytes []byte
	if valueBytes, err = json.Marshal(value); err != nil {
		return
	}

	add := r.client.SetNX(ctx.Ctx, r.key(key), valueBytes, expiration)

	if r.debug {
		fmt.Println(add.Args())
	}

	if add.Err() != nil {
		err = add.Err()
		return
	}

	if add.Val() {
		return
	}

	return varis.ErrorCacheKeyAlreadySet
}

func (r *redis) Delete(ctx *isession.Isession, key string) (err error) {
	defer func() {
		if err != nil {
			ctx.Error(isession.GetSource(), "Delete Redis", varis.CodeCacheErrorDelete, err)
		}
	}()

	del := r.client.Del(ctx.Ctx, r.key(key))

	if r.debug {
		fmt.Println(del.Args())
	}

	if del.Err() != nil {
		err = del.Err()
		return
	}

	if del.Val() == 0 {
		err = varis.ErrorCacheDelete
		return
	}

	return
}

func (r *redis) Close(ctx *isession.Isession) (err error) {
	ctx.Info("Close Redis")
	return r.client.Close()
}

func (r *redis) key(key string) string {
	return r.prefix + key
}
