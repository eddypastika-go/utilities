package redis

import (
	"context"
	"time"

	v8 "github.com/go-redis/redis/v8"
)

type client interface {
	Ping(ctx context.Context) *v8.StatusCmd
	Get(ctx context.Context, key string) *v8.StringCmd
	Set(ctx context.Context, key string, value interface{}, expiration time.Duration) *v8.StatusCmd
	SetNX(ctx context.Context, key string, value interface{}, expiration time.Duration) *v8.BoolCmd
	Del(ctx context.Context, keys ...string) *v8.IntCmd
	// unused, and maybe added more later
	//Incr(ctx context.Context, key string) *v8.IntCmd
	//IncrBy(ctx context.Context, key string, value int64) *v8.IntCmd
	//Decr(ctx context.Context, key string) *v8.IntCmd
	//DecrBy(ctx context.Context, key string, value int64) *v8.IntCmd
	//TTL(ctx context.Context, key string) *v8.DurationCmd
	//Expire(ctx context.Context, key string, expiration time.Duration) *v8.BoolCmd
	//ExpireAt(ctx context.Context, key string, tm time.Time) *v8.BoolCmd
	Close() error
}
