package redis

import (
	"time"
)

type Config struct {
	Debug  bool   `json:"debug"`
	Prefix string `json:"prefix"`
	Mode   Mode   `json:"mode"`
	// The network type, either tcp or unix.
	// Default is tcp.
	// Use by Single.
	Network string `json:"network"`
	// A seed list of host:port addresses of cluster nodes or sentinel nodes.
	// Use by Single, Cluster and Sentinel.
	Addresses []string `json:"addresses"`
	// The master name.
	// Use by Sentinel.
	MasterName string `json:"masterName"`
	// Use the specified Username to authenticate the current connection
	// with one of the connections defined in the ACL list when connecting
	// to a redis 6.0 instance, or greater, that is using the redis ACL system.
	// Use by Single, Cluster and Sentinel.
	Username string `json:"username"`
	// Optional password. Must match the password specified in the
	// requirepass server configuration Option (if connecting to a redis 5.0 instance, or lower),
	// or the User Password when connecting to a redis 6.0 instance, or greater,
	// that is using the redis ACL system.
	// Use by Single, Cluster and Sentinel.
	Password string `json:"password"`
	// Sentinel password from "requirepass <password>" (if enabled) in Sentinel configuration.
	// Use by Sentinel.
	SentinelPassword string `json:"sentinelPassword"`
	// Database to be selected after connecting to the server.
	// Use by Single and Sentinel.
	DB int `json:"db"`
	// Maximum number of retries before giving up.
	// Default is 3 retries.
	// Use by Single, Cluster and Sentinel.
	MaxRetries int `json:"maxRetries"`
	// Minimum backoff between each retry.
	// Default is 8 milliseconds; -1 disables backoff.
	// Use by Single, Cluster and Sentinel.
	MinRetryBackoff time.Duration `json:"minRetryBackoff"`
	// Maximum backoff between each retry.
	// Default is 512 milliseconds; -1 disables backoff.
	// Use by Single, Cluster and Sentinel.
	MaxRetryBackoff time.Duration `json:"maxRetryBackoff"`
	// Dial timeout for establishing new connections.
	// Default is 5 seconds.
	// Use by Single, Cluster and Sentinel.
	DialTimeout time.Duration `json:"dialTimeout"`
	// Timeout for socket reads. If reached, commands will fail
	// with a timeout instead of blocking. Use value -1 for no timeout and 0 for default.
	// Default is 3 seconds.
	// Use by Single, Cluster and Sentinel.
	ReadTimeout time.Duration `json:"readTimeout"`
	// Timeout for socket writes. If reached, commands will fail
	// with a timeout instead of blocking.
	// Default is ReadTimeout.
	// Use by Single, Cluster and Sentinel.
	WriteTimeout time.Duration `json:"writeTimeout"`
	// Maximum number of socket connections.
	// Default is 10 connections per every CPU as reported by runtime.NumCPU.
	// Use by Single, Cluster and Sentinel.
	PoolSize int `json:"poolSize"`
	// Minimum number of idle connections which is useful when establishing
	// new connection is slow.
	// Use by Single, Cluster and Sentinel.
	MinIdleConnections int `json:"minIdleConnections"`
	// Connection age at which client retires (closes) the connection.
	// Default is to not close aged connections.
	// Use by Single, Cluster and Sentinel.
	MaxConnectionAge time.Duration `json:"maxConnectionAge"`
	// Amount of time client waits for connection if all connections
	// are busy before returning an error.
	// Default is ReadTimeout + 1 second.
	// Use by Single, Cluster and Sentinel.
	PoolTimeout time.Duration `json:"poolTimeout"`
	// Amount of time after which client closes idle connections.
	// Should be less than server's timeout.
	// Default is 5 minutes. -1 disables idle timeout check.
	// Use by Single, Cluster and Sentinel.
	IdleTimeout time.Duration `json:"idleTimeout"`
	// Frequency of idle checks made by idle connections reaper.
	// Default is 1 minute. -1 disables idle connections reaper,
	// but idle connections are still discarded by the client
	// if IdleTimeout is set.
	// Use by Single, Cluster and Sentinel.
	IdleCheckFrequency time.Duration `json:"idleCheckFrequency"`
	// Enables read only queries on slave nodes.
	// Use by Cluster.
	ReadOnly bool `json:"readOnly"`
	// The maximum number of retries before giving up. Command is retried
	// on network errors and MOVED/ASK redirects.
	// Default is 3 retries.
	// Use by cluster.
	// Use by Cluster.
	MaxRedirects int `json:"maxRedirects"`
	// Allows routing read-only commands to the closest master or slave node.
	// It automatically enables ReadOnly.
	// Use by Cluster and Sentinel.
	RouteByLatency bool `json:"routeByLatency"`
	// Allows routing read-only commands to the random master or slave node.
	// It automatically enables ReadOnly.
	// Use by Cluster and Sentinel.
	RouteRandomly bool `json:"routeRandomly"`
	// Route all commands to slave read-only nodes.
	// Use by Sentinel.
	SlaveOnly bool `json:"slaveOnly"`
}

type Mode string

const (
	Single   Mode = "single"
	Cluster  Mode = "cluster"
	Sentinel Mode = "sentinel"
)

func (m Mode) String() string {
	return string(m)
}
