package mware

import (
	"context"
	"time"

	"gitlab.com/eddypastika-go/utilities/common/typeconv"

	"github.com/labstack/echo/v4"

	"gitlab.com/eddypastika-go/utilities/entitis"
	"gitlab.com/eddypastika-go/utilities/isession"
	"gitlab.com/eddypastika-go/utilities/logger"
	"gitlab.com/eddypastika-go/utilities/varis"
	"google.golang.org/grpc"
)

func GrpcInterceptor(logs logger.Logger, appName, appVersion string, port int) grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
		is := &isession.Isession{
			Logger:      logs,
			RequestTime: time.Now(),
			AppName:     appName,
			AppVersion:  appVersion,
			URL:         info.FullMethod,
			Method:      "gRPC",
			Port:        port,
			Request:     typeconv.ToJSONIndent(req),
		}

		c := context.WithValue(ctx, isession.KeyIsession, is)
		return handler(c, req)
	}
}

func ErrorHandler(ctx *isession.Isession, source logger.Field, msgLog string, err error) (string, string) {
	response := entitis.Response{
		Status:  varis.CodeGRPCError,
		Message: varis.ErrorGRPC.Error(),
	}

	if he, ok := err.(*echo.HTTPError); ok {
		response.Message = he.Error()
	} else {
		response.Message = err.Error()
	}

	ctx.Error(source, msgLog, response.Status, err)

	return response.Status, response.Message
}
