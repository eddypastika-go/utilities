package mware

import (
	"net/http"
	"strings"

	"gitlab.com/eddypastika-go/utilities/isession"

	"gitlab.com/eddypastika-go/utilities/entitis"

	"gitlab.com/eddypastika-go/utilities/logger"

	"gitlab.com/eddypastika-go/utilities/varis"

	"github.com/labstack/echo/v4"
	"gitlab.com/eddypastika-go/utilities/token"
)

func DecodeToken(logs logger.Logger, t token.Token) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			var (
				res      entitis.Response
				httpcode int
			)

			tokenString := c.Request().Header.Get(varis.Authorization)
			tokenString = strings.ReplaceAll(tokenString, varis.Bearer, varis.Empty)

			if tokenString == "" {
				httpcode = http.StatusUnauthorized
				res.Status = varis.CodeUnauthorized
				res.Message = varis.ErrorAuthorizationNotFound.Error()
				errorLog(logs, isession.GetSource(), res.Status, res.Message, httpcode)

				return c.JSON(httpcode, res)
			}

			claims, err := t.DecodeToken(tokenString)
			if err != nil {
				httpcode = http.StatusUnauthorized
				res.Status = varis.CodeUnauthorized
				res.Message = varis.ErrorInvalidToken.Error()
				errorLog(logs, isession.GetSource(), res.Status, res.Message, httpcode)

				return c.JSON(httpcode, res)
			}

			c.Set(varis.TokenPayload, claims.User)
			return next(c)
		}
	}
}

func errorLog(logs logger.Logger, source logger.Field, rescode, err string, httpcode int) {
	msg := "Decode Token Util"

	logRecord := []logger.Field{
		{"rescode", rescode},
		{"httpcode", httpcode},
		{"error", err},
		source,
	}

	logs.Error(msg, logRecord...)
}
