package mware

import (
	"context"
	"net/http"
	"time"

	"gitlab.com/eddypastika-go/utilities/varis"

	"gitlab.com/eddypastika-go/utilities/logger"

	"github.com/labstack/echo/v4"
	"gitlab.com/eddypastika-go/utilities/isession"
	"gitlab.com/eddypastika-go/utilities/random"
)

const requestTime = "requestTime"

// EchoSessionMiddleware is mware for setting up isession when request coming
func EchoSessionMiddleware(appCode, appName, appVer, dbDialect string, logger logger.Logger) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			now := time.Now()
			c.Set(requestTime, now)

			if isession.SkipLogging(c.Request().URL.String()) {
				return next(c)
			}

			source := c.Request().Header.Get(varis.Source)
			svcCode := c.Request().Header.Get(varis.ServiceCode)
			svcSecret := c.Request().Header.Get(varis.ServiceSecret)
			xID := c.Request().Header.Get(echo.HeaderXRequestID)
			if len(xID) == 0 {
				xID = random.GenerateXID(appCode)
			}

			is := isession.Isession{
				Ctx:            context.Background(),
				Echo:           c,
				Logger:         logger,
				RequestTime:    now,
				XID:            xID,
				Source:         source,
				AppName:        appName,
				AppVersion:     appVer,
				DBDialect:      dbDialect,
				SrcIP:          c.RealIP(),
				URL:            c.Request().URL.String(),
				Method:         c.Request().Method,
				Header:         isession.FormatHeader(c),
				ServiceCode:    svcCode,
				ServiceSecret:  svcSecret,
				ResponseCode:   varis.CodeSuccess,
				HTTPStatusCode: http.StatusOK,
			}
			is.Info("Incoming Request")

			c.Set(isession.KeyIsession, &is)
			c.Response().Header().Set(echo.HeaderXRequestID, xID)

			return next(c)
		}
	}
}
