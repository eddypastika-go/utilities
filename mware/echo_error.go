package mware

import (
	"net/http"

	"gitlab.com/eddypastika-go/utilities/varis"

	"github.com/labstack/echo/v4"
	"gitlab.com/eddypastika-go/utilities/entitis"
)

func EchoErrorHandler(err error, c echo.Context) {
	var (
		response entitis.Response
		status   = varis.CodeGeneralError
		code     = http.StatusInternalServerError
		msg      = http.StatusText(code)
	)

	if he, ok := err.(*echo.HTTPError); ok {
		code = he.Code
		status = mapErrorCode(he.Code)
		msg = he.Message.(string)
	} else {
		msg = err.Error()
	}

	response.Status = status
	response.Message = msg

	_ = c.JSON(code, response)
}

func mapErrorCode(code int) (res string) {
	switch code {
	case http.StatusNotFound:
		res = varis.CodeNotFound
	case http.StatusInternalServerError, http.StatusBadGateway:
		res = varis.CodeServerError
	}

	return
}
