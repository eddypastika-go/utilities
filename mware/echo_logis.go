package mware

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/eddypastika-go/utilities/common"
	"gitlab.com/eddypastika-go/utilities/isession"
)

func LogisSkipper(c echo.Context) bool {
	return isession.SkipLogging(c.Request().URL.String())
}

func LogisHandler(c echo.Context, req, res []byte) {
	ctx := c.Get(isession.KeyIsession).(*isession.Isession)
	ctx.Request = common.FormatInterfaceLog(req)
	ctx.Response = common.FormatInterfaceLog(res)
	ctx.LOGIS("Logis")
}

func LogisMiddleware() middleware.BodyDumpConfig {
	return middleware.BodyDumpConfig{
		Skipper: LogisSkipper,
		Handler: LogisHandler,
	}
}
