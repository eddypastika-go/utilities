package secret

var secretKey = "seringmengeluhtapitakmauberpeluh"

type Secret interface {
	GenerateServiceCredential(svcName string) (code string, secret string)
	GeneratePassword(password string) (string, error)
	CheckPassword(inputPassword, hashedPassword string) error
	GenerateVerificationCode(max int) (string, error)
}

type secret struct {
	cost int
	key  string
}

func New() Secret {
	return &secret{cost: 10, key: secretKey}
}
