package secret

import "golang.org/x/crypto/bcrypt"

func (s *secret) GeneratePassword(pass string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(pass), s.cost)
	if err != nil {
		return "", err
	}
	hashed := string(bytes)

	return hashed, nil
}

func (s *secret) CheckPassword(inputPassword, hashedPassword string) error {
	return bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(inputPassword))
}
