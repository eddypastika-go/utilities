package secret

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/md5"
	crand "crypto/rand"
	"encoding/hex"
	"fmt"
	"io"
	"math/rand"
	"strings"
	"time"

	"gitlab.com/eddypastika-go/utilities/random"
)

func (s *secret) GenerateServiceCredential(svcName string) (code string, secret string) {
	rand.Seed(time.Now().UnixNano())
	randNumber := rand.Intn(2000-1000) + 1000

	svc := constructServiceName(svcName)
	code = strings.ToUpper(fmt.Sprintf("%s%s%v", svc, random.GetMixedNumbAndLetter(5), randNumber))
	secret = fmt.Sprintf("%x", encrypt([]byte(code), secretKey))

	return
}

func constructServiceName(svc string) (res string) {
	res = strings.ReplaceAll(svc, " ", "")

	if len(svc) > 5 {
		res = res[:5]
	}

	return
}

func createHash(key string) string {
	hasher := md5.New()
	hasher.Write([]byte(key))
	return hex.EncodeToString(hasher.Sum(nil))
}

func encrypt(data []byte, passphrase string) []byte {
	block, _ := aes.NewCipher([]byte(createHash(passphrase)))
	gcm, err := cipher.NewGCM(block)
	if err != nil {
		panic(err.Error())
	}
	nonce := make([]byte, gcm.NonceSize())
	if _, err = io.ReadFull(crand.Reader, nonce); err != nil {
		panic(err.Error())
	}
	ciphertext := gcm.Seal(nonce, nonce, data, nil)

	return ciphertext
}
